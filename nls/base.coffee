define(
  root:
    infoBar:
      select: "Select"

    proteinView:
      lockInfoTooltip: "Rotation is disabled. Double click to activate again."
)

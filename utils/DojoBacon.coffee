define ["bacon/dist/Bacon"],
(Bacon) ->

  fromDomNode: (target, eventName) ->
    Bacon.fromBinder (handler) ->
      canceller = listener(target, eventName, handler)
      canceller.remove.bind canceller

  fromWidget: (target, eventName) ->
    Bacon.fromBinder (handler) ->
      canceller = target.on eventName, handler
      canceller.remove.bind canceller

  fromButton: (button) ->
    @fromWidget(button, "click")

  fromCheckBox: (target, skipDuplicates) ->
    getter = target.get.bind target, "checked"
    result = @fromWidget(target, "change")
      .map(getter)
      .toProperty(getter())
    if (skipDuplicates) then result.skipDuplicates() else result

  fromFormWidget: (target, skipDuplicates) ->
    getter = target.get.bind target, "value"
    result = @fromWidget(target, "change")
      .map(getter)
      .toProperty(getter())
    if (skipDuplicates) then result.skipDuplicates() else result

  fromSelect: (target, skipDuplicates) ->
    getter = target.get.bind target, "value"
    result = @fromWidget(target, "change")
      .map(getter)
      .toProperty(getter())
    if (skipDuplicates) then result.skipDuplicates() else result

  withMultiSelectionInGrid: (grid) ->
    selectStream = @fromDomNode(grid.contentNode, "dgrid-select")
          .map((evt) -> {data: _.map(evt.rows, "data"), added: true})
    deselectStream = @fromDomNode(grid.contentNode, "dgrid-deselect")
          .map((evt) -> {data: _.map(evt.rows, "data"), added: false})

    selectStream.merge(deselectStream).scan [], (seed, newVals) ->
      if (newVals.added)
        _.each newVals.data, (val) ->
          if (seed.indexOf(val) is -1) then seed.push val
        seed
      else
        _.filter(seed, (oldVal) -> newVals.data.indexOf(oldVal) is -1)

define [],
() ->

  skip: (numArgs, func) ->
    lastArgs = []
    lastResult = undefined
    () ->
      i = 0
      while (i < numArgs)
        if (lastArgs[i] isnt arguments[i])
          lastResult = func.apply(@, arguments)
          lastArgs = arguments
          return lastResult
        i++
      lastResult

  indexOf: (xs, func) ->
    i = 0
    l = xs.length
    while (i < l)
      if (func(xs[i], i, xs)) then return i
      i++
    return -1

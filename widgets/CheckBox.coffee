define ["dojo/_base/declare",
        "dijit/_Widget",
        "xstyle/css!./../resources/checkbox.css"],
(declare,
 _Widget) ->

  declare _Widget, {

    width: 16
    height: 16

    checked: false

    constructor: ({parent, state, label}) ->
      @container = parent.append("g")
        .classed("check-box", true)
        .on("click", @mouseClickHandler.bind(@))

      @p1 = @container.append("path").attr("d", "M 11 4 C 7.1 4 4 7.1 4 11 L 4 39 4 39 C 4 42.9 7.1 46 11 46 L 39 46 C 42.9 46 46 42.9 46 39 L 46 11 46 11 C 46 7.1 42.9 4 39 4 L 11 4")
        .attr("transform", "matrix(0.4 0 0 0.4 0 -2)")
        .classed("unchecked-external-path", true)
      @p2 = @container.append("path").attr("d", "M 44 39 C 44 41.8 41.8 44 39 44 L 11 44 C 8.2 44 6 41.8 6 39 L 6 11 C 6 8.2 8.2 6 11 6 L 39 6 C 41.8 6 44 8.2 44 11")
        .attr("transform", "matrix(0.4 0 0 0.4 0 -2)")
        .classed("unchecked-internal-path", true)

      @p3 = @container.append("path").attr("d", "M 11 4 C 7.1 4 4 7.1 4 11 L 4 39 C 4 42.9 7.1 46 11 46 L 39 46 C 42.9 46 46 42.9 46 39 L 46 15 L 44 17.3 L 44 39 C 44 41.8 41.8 44 39 44 L 11 44 C 8.2 44 6 41.8 6 39 L 6 11 C 6 8.2 8.2 6 11 6 L 37.4 6 L 39 4 L 11 4 z M 43.2 7.7 L 23.9 30.5 L 15.7 22.9 L 14.4 24.4 L 23.3 32.7 L 24.0 33.4 L 24.75 32.6 L 44.75 9.0 L 43.2 7.7 z")
        .attr("transform", "matrix(0.4 0 0 0.4 0 -2)")
        .classed("checked-path", true)

      @label = label
      @text = @container.append("text")
        .attr("x", @width + 5)
        .attr("y", @height / 2)
      @text.text(label)

      @setState state

    getState: -> @checked

    setState: (newValue) ->
      @checked = newValue
      if (@checked)
        @p1.style("fill", "white")
        @p2.style("display", "none")
        @p3.style("display", "inline")
      else
        @p1.style("fill", "black")
        @p2.style("display", "inline")
        @p3.style("display", "none")

    mouseClickHandler: ->
      @setState !@checked
      @onMouseClick {label: @label, state: @checked}

    onMouseClick: (evt) ->

    getWidth: -> @width
    getHeight: -> @height

    move: (x, y) ->
      @container.attr("transform", "translate(#{x}, #{y})")
      @

    fill: (color) ->
      @text.style("fill", color)
      @

    destroy: ->
      @inherited arguments
  }

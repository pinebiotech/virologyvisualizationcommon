define(["dojo/_base/declare", "dojo/_base/lang", "dojo/dom-geometry", "dojo/on", "dojo/dom-style",
        "dijit/_Widget", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin",
        "pv/pv", "pv/color", "pv/gl-matrix", "pv/gfx/label", "pv/utils",
        "dojo/i18n!./../nls/base",
        "dojo/text!./../templates/ProteinViewer.html",
        "xstyle/css!./../resources/protein-view.css"],
(declare, lang, domGeometry, listener, domStyle,
 _Widget, _TemplatedMixin, _WidgetsInTemplateMixin,
 pv, pvColor, matrix, Label, pvutils,
 strings,
 template) ->

  v3 = matrix.vec3

  fontProps =
    fontSize: 32
    fontColor: "#000"

  TextLabel = (gl, canvas, context, pos, text, options) ->
    Label.call @, gl, canvas, context, pos, text, options

  TextLabel.addMeTo = (viewer, name, text, pos, options) ->
      label = new TextLabel viewer._canvas.gl(), viewer._textureCanvas, viewer._2dcontext, pos, text, options
      viewer.add name, label

  pvutils.derive TextLabel, Label, {
    _prepareText: (canvas, ctx, text) ->
      Label.prototype._prepareText.call @, canvas, ctx, text
      @_yScale = 1.0
  }

  findResidueByPosition = (structure, position) ->
    chains = structure.chains()
    chainIdx = -1
    length = 0
    while (++chainIdx < chains.length)
      chain = chains[chainIdx]
      residues = chain.residues()
      if (position < length + residues.length)
        return residues[position - length]
      else
        length += residues.length

  findAbsolutePosition = (structure, atom) ->
    residue = atom.residue()
    chain = residue.chain()
    length = 0
    chainIdx = -1
    while (++chainIdx < structure.chains().length)
      curChain = structure.chains()[chainIdx]
      if (curChain.name() is chain.name())
        return length + residue.index()
      else
        length += curChain.residues().length


  declare [_Widget, _TemplatedMixin, _WidgetsInTemplateMixin], {
    templateString: template

    _renderingType: "cartoon"
    defunc: false

    constructor: ->
      lang.mixin @, strings.proteinView

    initIfNeed: ->
      unless (@initiated)
        @initiated = true
        proteinPanelSize = domGeometry.position @domNode
        @options =
          width: proteinPanelSize.w, height: proteinPanelSize.h,
          antialias: true, quality: "high"
        try
          @viewer = pv.Viewer @domNode, @options
          @viewer.on "viewpointChanged", @onCameraChange.bind @
          @viewer.on "mousemove", @mouseMoveHandler.bind @
          @viewer.on "click", @aminoacidClickHandler.bind @

          domStyle.set @lockInfo, "right", @rightOffset + "%"
          @locked = false
          listener @viewer._canvas.domElement(), "dblclick", @dblClickListener.bind @
        catch e
          console.warn e
          @defunc = true

    setHeight: (h) ->
      @initIfNeed()
      if (@defunc) then return
      @viewer.resize @options.width, h || @options.height

    renderProtein: (name, structure) ->
      @initIfNeed()
      if (@defunc) then return
      @name = @title.innerHTML = name || ""

      @cartoon = @viewer.renderAs "protein", structure, @_renderingType
      @viewer.centerOn structure
      @viewer.autoZoom()
      @currentStructure = structure
      @currentCenter = @viewer.center()
      @currentZoom = @viewer._cam._zoom

    colorBy: (colorOperation, skipRedraw) ->
      if (@defunc) then return
      @cartoon.colorBy colorOperation
      unless (skipRedraw) then @viewer.requestRedraw()

    rotateToPosition: (position, label) ->
      if (@defunc) then return
      residue = findResidueByPosition @currentStructure, position
      if (residue)
        atom = residue.atom(0)
        unless (@locked)
          x0 = v3.fromValues.apply(v3, @currentCenter)
          x = v3.fromValues.apply(v3, atom.pos())
          vec = v3.sub(v3.create(), x, x0)
          eye = [2 * x[0] - x0[0], 2 * x[1] - x0[1], 2 * x[2] - x0[2]]
          len = Math.sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2])
          R = matrix.mat4.lookAt(matrix.mat4.create(), eye, x, [vec[0] + len, vec[1] + len, vec[2] + len])
          @viewer.setRotation R, 50
        if (label?) then @showLabel label, atom

    setCamera: (newCamera) ->
      if (@defunc) then return
      @viewer.setCamera newCamera._rotation, newCamera.center(), newCamera._zoom

    showLabel: (label, atom) ->
      @viewer.rm "hovered-aminoacid"
      TextLabel.addMeTo @viewer, "hovered-aminoacid", label, atom.pos(), fontProps

    setRenderingType: (newType) ->
      if (@_renderingType isnt newType)
        @_renderingType = newType
        structure = @currentStructure
        if (structure?)
          camera = @viewer._cam
          @clear ""
          @viewer.requestRedraw()
          @renderProtein @name, structure
          @setCamera camera
          true
        else
          false
      else
        false

    resizeImage: (w, h) ->
      @viewer.resize(w, h) if (@viewer?)

    mouseMoveHandler: (evt) ->
      rect = @viewer.boundingClientRect()
      picked = @viewer.pick {x: evt.clientX - rect.left, y: evt.clientY - rect.top}
      if (picked and @prevPicked isnt picked)
        @prevPicked = picked
        @onMouseMove {atom: picked.target(), pos: findAbsolutePosition(@currentStructure, picked.target())}

    aminoacidClickHandler: (x, evt) ->
      rect = @viewer.boundingClientRect()
      picked = @viewer.pick {x: evt.clientX - rect.left, y: evt.clientY - rect.top}
      if (picked)
        @onMouseClick {atom: picked.target(), pos: findAbsolutePosition(@currentStructure, picked.target())}

    _getLockAttr: -> @locked

    _setLockAttr: (newValue) ->
      @locked = newValue
      domStyle.set(@lockInfo, "display", if (newValue) then "block" else "none")

    dblClickListener: ->
      @set "lock", !@locked
      @onLockChange @locked

    onLockChange: (locked) ->

    onMouseMove: (evt) ->
    onMouseClick: (evt) ->

    setError: (err) ->
      @title.innerHTML = err

    clear: (title) ->
      @initIfNeed()
      if (@defunc) then return
      @viewer.clear()
      @viewer.setCamera matrix.mat3.identity(matrix.mat3.create()), [0, 0, 0], 1
      @viewer.requestRedraw()
      @title.innerHTML = title
      @currentStructure = undefined

    isEmpty: -> @currentStructure is undefined

    onCameraChange: (camera) ->
  }
)

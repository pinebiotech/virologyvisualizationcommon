define(["dojo/_base/declare", "dojo/_base/lang", "dojo/dom-geometry", "dojo/dom-style", "dojo/dom-class", "dojo/dom-construct", "dojo/on", "dojo/string",
        "dijit/_Widget", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin",
        "./CheckBox",
        "dojo/text!./../templates/InfoBar.html",
        "dojo/i18n!./../nls/base",
        "xstyle/css!./../resources/info-bar.css"],
(declare, lang, domGeometry, domStyle, domClass, domConstruct, listener, string,
 _Widget, _TemplatedMixin, _WidgetsInTemplateMixin,
 CheckBox,
 template,
 strings) ->

  declare [_Widget, _TemplatedMixin, _WidgetsInTemplateMixin], {
    templateString: template

    constructor: ->
      lang.mixin @, strings.infoBar
      @state = false

    postCreate: ->
      if (@alignRight) then domStyle.set @domNode, "right", "0"
      if (@percentWidth?) then domStyle.set @domNode, "width", @percentWidth + "%"

      @checkbox = new CheckBox({parent: d3.select(@checkboxArea), state: false, label: @select})
      @checkbox.move 0, 5

      @additionalButtons = _.map @buttons, (d) =>
        td = domConstruct.create "td", {
          className: "fake-button",
          innerHTML: d.label,
          buttonId: d.id
        }, @tableRow, "last"
        domConstruct.create d.tagName, {className: d.className}, td, "first"
        listener td, "click", @buttonClickHandler.bind(@, d.id)
        td

      listener @selectButton, "click", @clickListener.bind @

    _getStateAttr: -> @state
    _setStateAttr: (newValue) ->
      @checkbox.setState newValue
      @state = newValue

    _getInfoAttr: -> @info
    _setInfoAttr: (info) ->
      if (info?)
        @textInfo.innerHTML = string.substitute @infoPattern, info
      else
        @textInfo.innerHTML = "-"
      @info = info

    clickListener: ->
      @set "state", !@state
      @onSelected {state: @state, info: @info}

    onSelected: ->

    getButton: (id) ->
      _.find @additionalButtons, (b) -> b.getAttribute("buttonId") is id

    buttonClickHandler: (id) ->
      @onButtonListener {button: id, state: @state, info: @info}

    onButtonListener: (evt) ->
  }
)
